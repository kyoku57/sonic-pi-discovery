# Unclassified

## External resources

http://mathartung.xyz/siteICN/sonicpi_a1.html

```ruby
live_loop :beat do
  sample :loop_industrial, beat_stretch:1, amp: 1
  use_synth :blade
  [1,3,4,5].each do |d|
    sample :drum_heavy_kick, amp: 2
    (range -2,2).each do |i|
      play_chord (chord_degree d, :c, :minor, 1, invert:i)
      sleep 0.25
    end
  end
end

live_loop :cymbal do
  sample :drum_cymbal_soft
  play 1
  sleep 1
end

live_loop :lead do
  use_synth :chiplead
  [1,1,3,1,6,6,5,7].each do |d|
    (range -1,1).each do |i|
      play_chord (chord_degree d, :c, :minor, 1, invert:i)
      sleep 0.25
    end
  end
end 
```

## Bells

```ruby
loop do
  sample :perc_bell, rate: (rrand 0.125, 1.5)
  sleep rrand(0, 2)
end
``` 


## Others to see
```ruby
cutoff:  
lpf:  
sample :loop_amen, lpf: 80, lpf_attack:1, lpf_attack_level:100  
hpf:  
```

## Some musics

Breathe :  
https://gist.github.com/mribica/91db9484581cd70a91c1

```ruby
use_bpm 130
base = 70

live_loop :theme do
  use_synth :prophet

  with_fx :distortion do
    "2--2--5-2-5-4--2-4--1--1--".split("").each do |n|
      sleep 0.5 and next if n == "-"
      play base + n.to_i, amp: 1, cutoff: 90, sustain: 0.5
    end
  end
end

live_loop :bass do
  sync :theme
  sample :bass_dnb_f, cutoff: 80, amp: 4, beat_duration: 4
  sleep 4
end

live_loop :drums do
  sync :theme
  sample :loop_amen_full, amp: 2, beat_stretch: 16, cutoff: 130
  sleep 16
end

live_loop :sword do
  sync :theme
  # Wu-Tang sword fight sample
  #sample '~/wu-sword.wav', start: 0.4, finish: 0.6, amp: 2, beat_stretch: 18
  sleep 4
end
```

Mario Theme :   
https://gist.github.com/xavriley/87ef7548039d1ee301bb

```ruby
comment do
  # transcribed from the MML notation here: http://www.mmlshare.com/tracks/view/403
  #
  # Sonic Pi currently has a size limit of about 9k which is a known issue (#102).
  # I've kept the comments up here to get around that as comment blocks don't get
  # sent to the interpreter. Some of the layout here is an exercise in reducing bytes.
  # I'm using Ruby's stabby lambda syntax ( -> { ... } ) in case you want to google it :)
  #
  # THIS HAS ONLY BEEN TESTED ON A MAC - on an RaspberryPi you might want to change it to
  # use_bpm 60
  #
  # Regarding the choice of an FM synth for drums:
  # You could use a noise synth here, but I think the NES sound
  # chip would have used something like this FM as the character
  # of the noise would change with different notes which I'm making
  # use of in drum_pattern_b
end
use_debug false # help RPi performance
use_bpm 100
use_synth :pulse
use_synth_defaults release: 0.2, mod_rate: 5, amp: 0.6
define :structure do |i,a,b,c,d|
  1.times { i.call }
  loop do
    2.times { a.call }
    2.times { b.call }
    1.times { c.call }
    2.times { a.call }
    2.times { d.call }
    1.times { c.call }
    1.times { d.call }
  end
end

in_thread do
  intro = -> { play_pattern_timed([:e5,:e5,nil,:e5,nil,:c5,:e5,nil,
                      :g5,nil,nil,nil,nil,nil,nil,nil], [0.25]) }
  theme_a = -> {
    play_pattern_timed([:c5,nil,nil,:g4,nil,nil,:e4,nil,
                        nil,:a4,nil,:b4,nil,:Bb4,:a4,nil], [0.25])
    play_pattern_timed([:g4,:e5,:g5], [1/3.0]) # minim triplets
    play_pattern_timed([:a5,nil,:f5,:g5,
                        nil,:e5,nil,:c5,
                        :d5,:b4,nil,nil], [0.25]) }
  theme_b = -> {
    play_pattern_timed([nil,nil,:g5,:fs5,:f5,:ds5,nil,:e5,
                        nil,:gs4,:a4,:c5,nil,:a4,:c5,:d5,
                        nil,nil,:g5,:fs5,:f5,:ds5,nil,:e5,
                        nil,:c6,nil,:c6,:c6,nil,nil,nil,
                        nil,nil,:g5,:fs5,:f5,:ds5,nil,:e5,
                        nil,:gs4,:a4,:c5,nil,:a4,:c5,:d5,
                        nil,nil,:ds5,nil,nil,:d5,nil,nil,
                        :c5,nil,nil,nil,nil,nil,nil,nil], [0.25]) }
  theme_c = -> {
    play_pattern_timed([:c5,:c5,nil,:c5,nil,:c5,:d5,nil,
                      :e5,:c5,nil,:a4,:g4,nil,nil,nil,
                      :c5,:c5,nil,:c5,nil,:c5,:d5,:e5,
                      nil,nil,nil,nil,nil,nil,nil,nil,
                      :c5,:c5,nil,:c5,nil,:c5,:d5,nil,
                      :e5,:c5,nil,:a4,:g4,nil,nil,nil,
                      :e5,:e5,nil,:e5,nil,:c5,:e5,nil,
                      :g5,nil,nil,nil,nil,nil,nil,nil], [0.25]) } 
  theme_d = -> {
    play_pattern_timed([:e5,:c5,nil,:g4,nil,nil,:gs4,nil,
                        :a4,:f5,nil,:f5,:a4,nil,nil,nil], [0.25])
    play_pattern_timed([:b4,:a5,:a5,
                        :a5,:g5,:f5], [1/3.0])
    play_pattern_timed([:e5,:c5,nil,:a4,:g4,nil,nil,nil], [0.25])
    play_pattern_timed([:e5,:c5,nil,:g4,nil,nil,:gs4,nil,
                        :a4,:f5,nil,:f5,:a4,nil,nil,nil,
                        :b4,:f5,nil,:f5], [0.25])
    play_pattern_timed([:f5,:e5,:d5], [1/3.0])
    play_pattern_timed([:g5,:e5,nil,:e5,:c5,nil,nil,nil], [0.25]) }

  structure(intro, theme_a, theme_b, theme_c, theme_d)
end

in_thread do
  intro = -> { play_pattern_timed([:fs4,:fs4,nil,:fs4,nil,:fs4,:fs4,nil,
                      :b4,nil,nil,nil,:g4,nil,nil,nil], [0.25]) }
  theme_a = -> {
    play_pattern_timed([:e4,nil,nil,:c4,nil,nil,:g3,nil,
                        nil,:c4,nil,:d4,nil,:Db4,:c4,nil], [0.25])
    play_pattern_timed([:c4,:g4,:b4], [1/3.0])
    play_pattern_timed([:c5,nil,:a4,:b4,
                        nil,:a4,nil,:e4,
                        :f4,:d4,nil,nil], [0.25]) } 
  theme_b = -> {
    play_pattern_timed([nil,nil,:e5,:ds5,:d5,:b4,nil,:c5,
                        nil,:e4,:f4,:g4,nil,:c4,:e4,:f4,
                        nil,nil,:e5,:ds5,:d5,:b4,nil,:c5,
                        nil,:f5,nil,:f5,:f5,nil,nil,nil,
                        nil,nil,:e5,:ds5,:d5,:b4,nil,:c5,
                        nil,:e4,:f4,:g4,nil,:c4,:e4,:f4,
                        nil,nil,:gs4,nil,nil,:f4,nil,nil,
                        :e4,nil,nil,nil,nil,nil,nil,nil], [0.25]) }
  theme_c = -> { 
    play_pattern_timed([:gs4,:gs4,nil,:gs4,nil,:gs4,:as4,nil,
                        :g4,:e4,nil,:e4,:c4,nil,nil,nil,
                        :gs4,:gs4,nil,:gs4,nil,:gs4,:as4,:g4,
                        nil,nil,nil,nil,nil,nil,nil,nil,
                        :gs4,:gs4,nil,:gs4,nil,:gs4,:as4,nil,
                        :g4,:e4,nil,:e4,:c4,nil,nil,nil,
                        :fs4,:fs4,nil,:fs4,nil,:fs4,:fs4,nil,
                        :b4,nil,nil,nil,:g4,nil,nil,nil], [0.25]) }
  theme_d = -> {
    play_pattern_timed([:c5,:a4,nil,:e4,nil,nil,:e4,nil,
                        :f4,:c5,nil,:c5,:f4,nil,nil,nil], [0.25])
    play_pattern_timed([:g4,:f5,:f5,
                        :f5,:e5,:d5], [1/3.0])
    play_pattern_timed([:c5,:a4,nil,:f4,:e4,nil,nil,nil], [0.25])
    play_pattern_timed([:c5,:a4,nil,:e4,nil,nil,:e4,nil,
                        :f4,:c5,nil,:c5,:f4,nil,nil,nil,
                        :g4,:d5,nil,:d5], [0.25])
    play_pattern_timed([:d5,:c5,:b4], [1/3.0])
    play_pattern_timed([:c5,nil,nil,nil,nil,nil,nil,nil], [0.25]) }

  structure(intro, theme_a, theme_b, theme_c, theme_d)
end

in_thread do
  use_synth :tri
  use_synth_defaults attack: 0, sustain: 0.1, decay: 0.1, release: 0.1, amp: 0.4

  intro = -> { play_pattern_timed([:D4,:D4,nil,:D4,nil,:D4,:D4,nil,
                      :G3,nil,nil,nil,:G4,nil,nil,nil], [0.25]) }
  theme_a = -> {
    play_pattern_timed([:G4,nil,nil,:E4,nil,nil,:C4,nil,
                        nil,:F4,nil,:G4,nil,:Gb4,:F4,nil], [0.25])
    play_pattern_timed([:E4,:C4,:E4], [1/3.0])
    play_pattern_timed([:F4,nil,:D4,:E4,
                        nil,:C4,nil,:A3,
                        :B3,:G3,nil,nil], [0.25]) }
  theme_b = -> {
    play_pattern_timed([:C3,nil,nil,:G3,nil,nil,:C3,nil,
                        :F3,nil,nil,:C3,:C3,nil,:F3,nil,
                        :C3,nil,nil,:E3,nil,nil,:G3,:C3,
                        nil,:G2,nil,:G2,:G2,nil,:G4,nil,
                        :C3,nil,nil,:G3,nil,nil,:C3,nil,
                        :F3,nil,nil,:C3,:C3,nil,:F3,nil,
                        :C3,nil,:Ab3,nil,nil,:Bb3,nil,nil,
                        :C3,nil,nil,:G2,:G2,nil,:C3,nil], [0.25]) }
  theme_c = -> { 
    3.times {
      play_pattern_timed([:gs4,nil,nil,:ds4,nil,nil,:gs4,nil,
                        :g4,nil,nil,:c4,nil,nil,:g4,nil], [0.25])
    }
    play_pattern_timed([:D4,:D4,nil,:D4,nil,:D4,:D4,nil,
                      :G3,nil,nil,nil,:G4,nil,nil,nil], [0.25]) }
  theme_d = -> {
    play_pattern_timed([:C3,nil,nil,:fs3,:g3,nil,:C3,nil,
                        :F3,nil,:F3,nil,:C3,:C3,:F3,nil,
                        :D3,nil,nil,:F3,:G3,nil,:B3,nil,
                        :G3,nil,:G3,nil,:C3,:C3,:G3,nil,
                        :C3,nil,nil,:fs3,:g3,nil,:C3,nil,
                        :F3,nil,:F3,nil,:C3,:C3,:F3,nil,
                        :G3,nil,nil,:G3], [0.25])
    play_pattern_timed([:G3,:A3,:B3], [1/3.0])
    play_pattern_timed([:C4,nil,:G3,nil,:C4,nil,nil,nil], [0.25]) }

  structure(intro, theme_a, theme_b, theme_c, theme_d)
end

in_thread do
  use_synth :fm
  use_synth_defaults divisor: 1.6666, attack: 0.0, depth: 1500, sustain: 0.05, release: 0.0

  ll = -> { play :a, sustain: 0.1; sleep 0.75 }
  l = -> { play :a, sustain: 0.1; sleep 0.5 }
  s = -> { play :a; sleep 0.25 }
  
  define :drum_pattern_a do
    [l,s,l,s,l,ll,l,s,s,s].map(&:call)
  end

  define :drum_pattern_b do
    play :b
    sleep 0.5
    play :a6
    sleep 0.3
    play :a7
    sleep 0.2
    play :a, sustain: 0.1
    sleep 0.5
    play :a6
    sleep 0.3
    play :a7
    sleep 0.2
  end

  define :drum_pattern_c do
    [ll,s,l,l].map(&:call)
  end

  with_fx :level, amp: 0.5 do
    1.times  { drum_pattern_a }
    loop do
      24.times { drum_pattern_b }
      4.times  { drum_pattern_a }
      8.times  { drum_pattern_b }
      16.times { drum_pattern_c }
      4.times  { drum_pattern_a }
      8.times  { drum_pattern_b }
    end
  end
end
```

Game of Thrones:  
https://soundcloud.com/guo-yu-1/game-of-thrones-sonic-pi

```ruby
### Game of Thrones Page 1~2
define :ch1 do |n,l|
  play n, release: 1.2*l,  amp: 2
  sleep l
end

define :ch2 do |n1, n2 ,l|
  play_chord [n1,n2], release: 1.2*l, amp: 2
  sleep l
end
define :ch3 do |n1, n2, n3 ,l|
  play_chord [n1,n2,n3], release: 1.2*l,  amp: 2
  sleep l
end
define :ch4 do |n1, n2, n3, n4 ,l|
  play_chord [n1,n2,n3,n4], release: 1.2*l,  amp: 2
  sleep l
end

define :bass do |n|
  play n, amp: 2, release: 1
  sleep 1.5
end

define :bassloop do |n,l|
  l.times do
    bass n
  end
end

define :ch232 do |n11,n12,l1, n21,n22,n23,l2, n31,n32,l3|
  ch2 n11, n12, l1
  ch3 n21, n22, n23, l2
  ch2 n31, n32, l3
end

define :ch2_7 do |n11,n12,l1, n21,n22,l2, n31,n32,l3, n41,n42,l4, n51,n52,l5, n61,n62,l6, n71,n72,l7|
  ch2 n11,n12,l1
  ch2 n21,n22,l2
  ch2 n31,n32,l3
  ch2 n41,n42,l4
  ch2 n51,n52,l5
  ch2 n61,n62,l6
  ch2 n71,n72,l7
end

use_bpm 80
live_loop :GOT do
  in_thread do
    ################## Melody ########################
    in_thread do
      with_fx :reverb do
        use_synth :pulse

        ## Page 1
        # Bar 1
        play_pattern_timed [:G5,:C5,:Ef5,:F5,:G5,:C5,:Ef5,:F5],[1.5,1.5,0.25,0.25,1,1,0.25,0.25]
        play :D5, attack: 0.3, sustain: 1.8, release: 1.7
        sleep 3
        sleep 3  # Bar 4  # Bar 7
        play_pattern_timed [:F5,:Bf4,:Ef5,:D5,:F5,:Bf4,:Ef5,:D5,:C5],[1.5,1.5,0.25,0.25,1,1.5,0.25,0.25,2.5]
        play_pattern_timed [:Af4,:F4,:C4,:F3,:G4],[0.5,0.5,0.5,1.5,1.5]
        play :C4, attack: 1, sustain: 2, release: 3
        sleep 4.5

        # Bar 12
        sleep 11.5

        play_pattern_timed [:C4,:D4,:Ef4,:F4],[0.125,0.125,0.125,0.125], amp: 0.5

        # Bar 15
        play_pattern_timed [:G4,:C4,:Ef4,:F4,:G4,:C4,:Ef4,:F4],[1.5,1.5,0.25,0.25,1,1,0.25,0.25]
        play :D4, attack: 0.3, sustain: 1.8, release: 1.7
        sleep 3

        ## Page 2
        # Bar 18 - 21
        sleep 3
        play_pattern_timed [:F4,:Bf3,:Ef4,:D4,:F4,:Bf3,:Ef4,:D4,:C4],[1.5,1.5,0.25,0.25,1,1.5,0.25,0.25,2.5]
        sleep 2.5
        play_pattern_timed [:C4,:D4,:Ef4,:F4],[0.125,0.125,0.125,0.125], amp: 0.5
        # Bar 24
        play_pattern_timed [:G4,:C4,:Ef4,:F4,:G4,:C4,:Ef4,:F4],[1.5,1.5,0.25,0.25,1,1,0.25,0.25]
        play :D4, attack: 0.3, sustain: 1.8, release: 1.7
        sleep 6
        # Bar 27
        play_pattern_timed [:F4,:Bf3,:Ef4,:D4,:F4,:Bf3,:Ef4,:D4,:C4],[1.5,1.5,0.25,0.25,1,1.5,0.25,0.25,2]
        # Bar 30
        sleep 3.5
        ch2_7 :G5,:Ef5, 1.5, :C5,:G4, 1.5, :C5,:Ef5, 0.25, :D5,:F5, 0.25, :G5,:Ef5, 1,  :C5,:G4, 1, :C5,:Ef5, 0.25
        ch2 :D5,:F5, 0.25

      end
    end

    ################## Bass1 ########################
    in_thread do
      with_fx :compressor do
        use_synth :beep

        ## Page 1
        # Bar 1
        play_pattern_timed [:G4,:C4,:Ef4,:F4,:G4,:C4,:Ef4,:F4],[0.5,0.5,0.25,0.25,0.5,0.5,0.25,0.25]
        play_pattern_timed [:G4,:C4,:Ef4,:F4,:G4,:C4,:Ef4,:F4],[0.5,0.5,0.25,0.25,0.5,0.5,0.25,0.25]
        play_pattern_timed [:D4,:G3,:Bf3,:C4,:D4,:G3,:Bf3,:C4],[0.5,0.5,0.25,0.25,0.5,0.5,0.25,0.25]
        # Bar 4
        play_pattern_timed [:D4,:G3,:Bf3,:C4,:D4,:G3,:Bf3,:C4],[0.5,0.5,0.25,0.25,0.5,0.5,0.25,0.25]
        play_pattern_timed [:F4,:Bf3,:D4,:Ef4,:F4,:Bf3,:D4,:Ef4],[0.5,0.5,0.25,0.25,0.5,0.5,0.25,0.25]
        play_pattern_timed [:F4,:Bf3,:D4,:Ef4,:F4,:Bf3,:D4,:Ef4],[0.5,0.5,0.25,0.25,0.5,0.5,0.25,0.25]
        # Bar 7
        play_pattern_timed [:C4,:F3,:Af3,:Bf3,:C4,:Bf3,:Af3],[0.5,0.5,0.25,0.25,0.5,0.5,0.5]
        play_pattern_timed [:F3,:C3,:Af2,:F2],[0.5,0.5,0.5,1.5]
        play :C2, release: 6
        sleep 4
        play :G2, release: 4
        sleep 2

        play_pattern_timed [:G3,:C2,:D3,:F3,:G3,:C3,:Ef3,:F3],[0.5,0.5,0.25,0.25,0.5,0.5,0.25,0.25]
        # Bar 12
        play :C3, release: 0.5
        play_pattern_timed [:G3,:C2,:D3,:F3,:G3,:C3,:Ef3,:F3],[0.5,0.5,0.25,0.25,0.5,0.5,0.25,0.25]
        play :C3, release: 0.5
        play_pattern_timed [:G3,:C2,:D3,:F3,:G3,:C3,:Ef3,:F3],[0.5,0.5,0.25,0.25,0.5,0.5,0.25,0.25]
        play :C3, release: 0.5
        play_pattern_timed [:G3,:C2,:D3,:F3,:G3,:C3,:Ef3,:F3],[0.5,0.5,0.25,0.25,0.5,0.5,0.25,0.25]
        # Bar 15
        play :C3, release: 0.5
        play_pattern_timed [:G3,:C2,:Ef3,:F3,:G3,:C3,:Ef3,:F3],[0.5,0.5,0.25,0.25,0.5,0.5,0.25,0.25]
        play :C3, release: 0.5
        play_pattern_timed [:G3,:C2,:Ef3,:F3,:G3,:C3,:Ef3,:F3],[0.5,0.5,0.25,0.25,0.5,0.5,0.25,0.25]
        play :D3, release: 0.5
        play_pattern_timed [:G2,:G1,:Bf2,:C3,:D3,:G2,:Bf2,:C3],[0.5,0.5,0.25,0.25,0.5,0.5,0.25,0.25]

        ## Page 2
        # Bar 18
        play :G2, release: 0.5
        play_pattern_timed [:D3,:G1,:Bf2,:C3,:D3,:G2,:Bf2,:C3],[0.5,0.5,0.25,0.25,0.5,0.5,0.25,0.25]
        play :Bf2, release: 0.5
        play_pattern_timed [:F3,:Bf1,:D3,:Ef3,:F3,:Bf2,:D3,:Ef3],[0.5,0.5,0.25,0.25,0.5,0.5,0.25,0.25]
        play :Bf2, release: 0.5
        play_pattern_timed [:F3,:Bf1,:D3,:Ef3,:F3,:Bf2,:D3,:Ef3],[0.5,0.5,0.25,0.25,0.5,0.5,0.25,0.25]
        # Bar 21
        play :F2, release: 0.5
        play_pattern_timed [:C3,:F1,:Af2,:Bf2,:C3,:F2,:Af2,:Bf2],[0.5,0.5,0.25,0.25,0.5,0.5,0.25,0.25]
        play_pattern_timed [:C3,:F1,:Af2,:Bf2,:C3,:F2,:Af2,:Bf2],[0.5,0.5,0.25,0.25,0.5,0.5,0.25,0.25]
        play :C3, release: 0.5
        play_pattern_timed [:G3,:C2,:Ef3,:F3,:G3,:C3,:Ef3,:F3],[0.5,0.5,0.25,0.25,0.5,0.5,0.25,0.25]
        # Bar 24
        play :C3, release: 0.5
        play_pattern_timed [:G3,:C2,:Ef3,:F3,:G3,:C3,:Ef3,:F3],[0.5,0.5,0.25,0.25,0.5,0.5,0.25,0.25]
        play :G2, release: 0.5
        play_pattern_timed [:D3,:G1,:Bf2,:C3,:D3,:G2,:Bf2,:C3],[0.5,0.5,0.25,0.25,0.5,0.5,0.25,0.25]
        play :G2, release: 0.5
        play_pattern_timed [:D3,:G1,:Bf2,:C3,:D3,:G2,:Bf2,:C3],[0.5,0.5,0.25,0.25,0.5,0.5,0.25,0.25]
        # Bar 27
        play :Bf2, release: 0.5
        play_pattern_timed [:F3,:Bf1,:D3,:Ef3,:F3,:Bf2,:D3,:Ef3],[0.5,0.5,0.25,0.25,0.5,0.5,0.25,0.25]
        play :Bf2, release: 0.5
        play_pattern_timed [:F3,:Bf1,:D3,:Ef3,:F3,:Bf2,:D3,:Ef3],[0.5,0.5,0.25,0.25,0.5,0.5,0.25,0.25]
        play_pattern_timed [:C3,:F1,:Af2,:Bf2,:C3,:F2,:Af2,:Bf2],[0.5,0.5,0.25,0.25,0.5,0.5,0.25,0.25]
        # Bar 30
        play_pattern_timed [:C3,:F1,:Af2,:Bf2,:C3,:F2,:Af2,:Bf2],[0.5,0.5,0.25,0.25,0.5,0.5,0.25,0.25]
        play :C3, release: 0.5
        play_pattern_timed [:G3,:C2,:Ef3,:F3,:G3,:C3,:Ef3,:F3],[0.5,0.5,0.25,0.25,0.5,0.5,0.25,0.25]
        play :C3, release: 0.5
        play_pattern_timed [:G3,:C2,:Ef3,:F3,:G3,:C3,:Ef3,:F3],[0.5,0.5,0.25,0.25,0.5,0.5,0.25,0.25]
      end
    end

    ################## Bass2 ########################
    in_thread do
      with_fx :reverb do
        with_fx :echo do
          use_synth :beep

          ## Page 1
          # Bar 1-2, 3-4, 5-6, 7
          bassloop :G4,4
          bassloop :D4,4
          bassloop :F4,4
          bassloop :C4,2
          sleep 9 # Bar 8-10
          bassloop :G3,8  # Bar 11-16

          2.times do
            bassloop :G3,4  # Bar 15-16 # Bar 23-24
            bassloop :D3,4 # Bar 17 - 18 ## Page 2 # Bar 25-26
            bassloop :F3,4 # Bar 19-20 # Bar 27-28
            bassloop :C3,4 # Bar 21-22 # Bar 29-30
          end
          bassloop :G3,4  # Bar 31-32
        end
      end
    end
  end
 sleep 96
 end

```

Stranger Things :  
https://www.youtube.com/watch?v=6Ue6-BPI-9k&feature=youtu.be

```ruby
##Stranger Things by Jan Albers

use_bpm 84

#defining individual parts

##introparts

#introchime
define :introchime do
  with_fx :reverb, room: 0.9 do
    with_fx :bpf, centre: 100, res: 0, amp: 0, amp_slide: 8 do |a|
      use_synth :pulse
      use_synth_defaults attack: 0.1, amp: 0.8, cutoff: 40
      in_thread do
        4.times do
          play_pattern_timed [:c6, :e6, :g6, :b6, :c7, :b6, :g6, :e6], [0.125]
          
        end
      end
      #volume modulation
      in_thread do
        sleep 0.5
        control a, amp: 1
      end
    end
  end
end

#intropad
define :intropad do
  with_fx :lpf, cutoff: 30, cutoff_slide: 4 do |c|
    use_synth :pulse
    use_synth_defaults sustain: 4, release: 2
    
    
    play_chord [:e3, :g3, :b3]
    control c, cutoff: 130
  end
end

##first hit parts

#bassattack
define :bassattack do
  use_synth :sine
  play :c1, sustain: 1, amp: 2, decay: 0.5
end

#firsthitpad

define :firsthitpad do
  with_fx :lpf, cutoff: 60 do
    use_synth :saw
    play_chord [:e2, :e3, :g3, :b3], release: 16, amp: 2
  end
end

#firsthitbass
define :firsthitbass do
  use_synth :tri
  use_synth_defaults sustain: 8, cutoff: 60
  play :c1
  sleep 8
  play :e1
end

##mainloop parts

#pedal 1
define :mainlooppedal1 do
  
  with_fx :lpf, cutoff: 70, cutoff_slide: 4 do |f|
    #osc1
    in_thread do
      live_loop :pedal1 do
        with_fx :lpf, cutoff: 80 do
          
          use_synth :saw
          use_synth_defaults release: 0.7
          
          
          
          play_pattern_timed [:c3, :e3, :g3, :b3, :c4, :b3, :g3, :e3], [0.25]
        end
      end
    end
    #osc2
    in_thread do
      live_loop :pedal2 do
        with_fx :lpf, cutoff: 60 do
          
          use_synth :prophet
          use_synth_defaults release: 0.7
          
          
          play_pattern_timed [:c3, :e3, :g3, :b3, :c4, :b3, :g3, :e3], [0.25]
        end
      end
    end
    #cutoff modulation
    in_thread do
      sleep 4
      control f, cutoff: 80
      sleep 8
      control f, cutoff: 100
      sleep 28
      control f, cutoff: 70
      sleep 4
      control f, cutoff: 55
      sleep 4
      control f, cutoff: 30
      sleep 4
      control f, cutoff: 0
    end
  end
end

#kick
define :kick1 do
  with_fx :lpf, cutoff: 30 do
    38.times  do
      
      sample :bd_ada, amp: 8
      sleep 0.25
      sample :bd_ada, amp: 8
      sleep 0.75
    end
  end
end

##secondhit parts

#secondhitchoir
define :secondhitchoir do
  with_fx :bpf, centre: 80, res: 0.8 do
    use_synth :hollow
    use_synth_defaults sustain: 8, release: 4, cutoff: 130, amp: 2
    
    play_chord [:e4, :g4, :b5]
  end
end

#secondhitswell
define :secondhitswell do
  with_fx :lpf, cutoff: 70 do
    use_synth :dsaw
    
    play_chord [:e3, :b3, :e4, :b4], release: 4
    
  end
end


#secondhitbass
define :secondhitbass do
  with_fx :reverb, damp: 1 do
    with_fx :ixi_techno, cutoff_min: 60, cutoff_max: 80, phase: 8, phase_offset: 0.5 do
      use_synth :dsaw
      play :e1, sustain: 7, amp: 0.5
    end
  end
end

##thirdhit parts

#thirdhitbass1
define :thirdhitbass1 do
  with_fx :reverb, damp: 1 do
    with_fx :ixi_techno, cutoff_min: 60, cutoff_max: 80, phase: 8, phase_offset: 0.5 do
      use_synth :dsaw
      use_synth_defaults amp: 0.4
      play :c1, sustain: 7
      sleep 7
      play :d1, sustain: 1
      sleep 1
      play :e1, sustain: 7
      sleep 7
      play :d1, sustain: 1
      sleep 1
      play :c1, sustain: 6
      sleep 6
      play :d1, sustain: 0.75
      sleep 0.75
      play :c1, sustain: 0.75
      sleep 0.75
      play :b0, sustain: 4, release: 5
      
    end
  end
end

#thirdhitbass2
define :thirdhitbass2 do
  
  with_fx :distortion, distort: 0.95, amp: 0.5 do
    with_fx :lpf, cutoff: 60 do
      use_synth :piano
      play :c1, sustain: 7
      sleep 7
      play :d1, sustain: 1
      sleep 1
      play :e1, sustain: 7
      sleep 7
      play :d1, sustain: 1
      sleep 1
      play :c1, sustain: 6
      sleep 6
      play :d1, sustain: 0.75
      sleep 0.75
      play :c1, sustain: 0.75
      sleep 0.75
      play :b0, sustain: 4, release: 5
    end
  end
end

#thirdhitzither
define :thirdhitzither do
  with_fx :lpf, cutoff: 80 do
    use_synth :chiplead
    use_synth_defaults amp: 0.5
    
    play :c3, release: 8
    sleep 1
    play :c5, release: 7
    play :g5, release: 7
  end
end

#thirdhitchoir
define :thirdhitchoir do
  use_synth :dark_ambience
  use_synth_defaults sustain: 2, release: 6
  
  play_chord [:g4, :b4, :g5, :b5]
end

#thirdhitpad
define :thirdhitpad do
  with_fx :lpf, cutoff: 75 do
    use_synth :dsaw
    use_synth_defaults detune: 0.05, attack: 0.5, sustain: 4, release: 4, amp: 2
    play_chord [:c3, :g3, :b3, :g4]
    sleep 8
    play_chord [:b2, :e3, :g3, :g4]
  end
end

#thirdhitsweep
define :thirdhitsweep do
  with_fx :lpf, cutoff: 90, amp: 0.5 do
    use_synth :dsaw
    use_synth_defaults  detune: 0.05
    
    play :c1, sustain: 5, attack: 2,  release: 1
    play :c2, sustain: 5, attack: 2,  release: 1
    sleep 8
    play :e1, sustain: 4, attack: 2,  release: 1
    play :e2, sustain: 4, attack: 2,  release: 1
    sleep 7
    play :d1, sustain: 0.5, attack: 0.5, release: 0.25
    play :d2, sustain: 0.5, attack: 0.5, release: 0.25
    
  end
end


##outro

#outropad
define :outropad do
  use_synth :hollow
  use_synth_defaults res: 0.999, amp: 6
  
  play :c4, attack: 1, sustain: 4.5, release: 2
  play :g4, attack: 1, sustain: 4.5, release: 2
  sleep 6
  play :a4, attack: 1, sustain: 0.5, release: 2
  sleep 0.75
  play :g4, attack: 1, sustain: 0.5, release: 2
  sleep 0.75
  play :gb4, attack: 1, sustain: 5.5, release: 5
end








#defining arrangement blocks

#intro
define :intro do
  in_thread   do
    introchime
  end
  in_thread do
    intropad
  end
end

#mainloop
define :mainloop do
  in_thread do
    mainlooppedal1
  end
  in_thread do
    kick1
  end
end

#firsthit
define :firsthit do
  in_thread do
    bassattack
  end
  in_thread do
    firsthitpad
  end
  in_thread do
    firsthitbass
  end
end

#secondhit

define :secondhit do
  in_thread do
    secondhitchoir
    
  end
  in_thread do
    secondhitbass
  end
  in_thread do
    secondhitswell
  end
end

##thirdhit
define :thirdhit do
  in_thread do
    thirdhitbass1
  end
  in_thread do
    thirdhitbass2
  end
  in_thread do
    thirdhitzither
  end
  in_thread do
    thirdhitchoir
  end
  in_thread do
    thirdhitpad
  end
  in_thread do
    thirdhitsweep
  end
end

##outro
define :outro do
  outropad
end



#defining arrangement itself
define :arrangement do
  intro
  in_thread delay: 4 do
    mainloop
    firsthit
  end
  in_thread delay: 12 do
    secondhit
  end
  in_thread delay: 20 do
    thirdhit
  end
  in_thread delay: 36 do
    outro
  end
end


#play whole track

in_thread do
  arrangement
end

```

let it go:  
https://gist.github.com/danreedy/a0f0aa1ec2eb275c55a2