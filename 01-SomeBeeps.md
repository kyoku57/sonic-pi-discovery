# Some beep

## Play sound

The main command for playing sound is **play**

```ruby
play 60
```


## Play two sounds
```ruby
play 60
play 70
```
The two sound are mixed. We need to seperate them with **sleep** command

## Separate the two sounds
```ruby
play 60
sleep 0.5   
play 70
```

Now the sound is played with a 0.5 seconds between them.

## Play two named notes

American notation is
```
C ,D ,E ,F ,G  ,A ,B  
```
.. equivalent in Europe is  
```
Do,Ré,Mi,Fa,Sol,La,Si
```

You can replace number with named note

```ruby
play :C
sleep 0.5   
play :D
```
or precise octave to use
```ruby
play :C4
sleep 0.5   
play :D4
```

## Up to octave
```ruby
play :C5
sleep 0.5   
play :D5
```

## Up to Sharp and Bemol
```ruby
play :Cs5
sleep 0.5   
play :Db5
```

C Sharp = D Bemol ... of course !

```ruby
play :Cs5
sleep 0.5   
play :Eb5
```

Octave from 1 to 9 is audible .. 10 .. you should be a dog


## Play patterns

```ruby
play_pattern_timed([:a5,nil,:f5,:g5,
                    nil,:e5,nil,:c5,
                    :d5,:b4,nil,nil], 0.25)
```

You can use nil to mark a pause.