# Tuning sound and first sugars

## Amplitude

You can control amplitude with **amp** option

```ruby
play 70
sleep 1
play 70, amp: 0.5
sleep 1
play 70, amp: 2
```

## Balance

You can control balance with **balance** option

```ruby
play 70, pan: -1
sleep 1
play 70, pan: 0
sleep 1
play 70, pan: 1
```

The value is limited by 
- -1 for left
- 1 for right


## First loop 
```ruby
[-1,0,1].each do |i|
  play 70, pan: i
  sleep 1
end
``` 

## An other way
```ruby
(range -1,2).each do |i|
  play 70, pan: i
  sleep 1
end
```

## An other step
```ruby
(range -1,1.25,0.25).each do |i|
  play 70, pan: i
  sleep 0.25
end
```

## An other loop
```ruby
(range 60,72).each do |note|
    play note
    sleep 1
end
```

## Play a number of times
```ruby
5.times do
    play :C
    sleep 1
end
```

## Include debug
```ruby
(range 60,72).each do |note|
    print "my note is", i
    play note
    sleep 1
end
```

