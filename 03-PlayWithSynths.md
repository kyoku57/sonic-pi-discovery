# Play with synth

## First synths

The command **use_synth** permits us to change instrument.

```ruby
use_synth :prophet
play :C
sleep 0.25
play :E
sleep 0.25
play :G
```

```ruby
use_synth :fm
play :C
sleep 0.25
play :E
sleep 0.25
play :G
```

```ruby
use_synth :pulse
play :C
sleep 0.25
play :E
sleep 0.25
play :G
```

The instrument can be found in synthetiseur section of Sonic Pi.