# Enveloppe ADSR

## Release
```ruby
play 70, release: 1
sleep 2
play 70, release: 2
sleep 2
play 70, release: 0.2
sleep 2
```

## Attack
```ruby
play 70, attack: 1
sleep 5
play 70, attack: 2
sleep 5
play 70, attack: 0.2
```

## Attack and Release
```ruby
play 70, attack: 2, release: 2
```

## Sustain
```ruby
play 70, attack: 2, sustain: 1, release: 2
```

## Decay, attack_level and sustain_level
```ruby
use_synth :growl
play 70, attack: 1, attack_level: 2, decay: 1, sustain_level: 0.5, sustain: 1, release: 2
```

## decay_level
```ruby
use_synth :growl
play 70, attack: 2, attack_level: 2, decay: 2, decay_level: 0.2, sustain: 2, sustain_level: 1, release: 2
```


