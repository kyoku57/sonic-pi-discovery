# Random sequence

## Play a random sound

```ruby
play rrand(70, 90)
```

Play it more than one time...what's happened ?

```ruby
5.times do
  play rrand(70, 90)
  sleep 0.5
end
```

So what is the problem ? The rrand is pseudo-random


## Random seed

You can change the departure of random number

```ruby
use_random_seed 40
5.times do
  play rrand(70, 90)
  sleep 0.5
end
```

## RRand integer
```ruby
use_random_seed 40
5.times do
  play rrand_i(70, 90)
  sleep 0.5
end
```

## Choose
```ruby
use_random_seed 20
10.times do
  play choose([:C,:E,:G,:B])
  sleep 0.25
end
```

## rand and rand_i

To use it with option, you can use rand and rand_i that choose value between 0 and max value

```ruby
use_random_seed 20
10.times do
  play 70, amp: rand(3)
  sleep 0.25
end
```

```ruby
use_random_seed 20
10.times do
  play 70, amp: rand_i(3)
  sleep 0.25
end
```


## dice

Precise the number of face for your dice (Dé) and you will have a number between 1 and your number


## one_in

Chance to obtain the higher number of a dice. (p = 1 / number)