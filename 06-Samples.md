# Samples

## Read a sample

```ruby
sample :ambi_lunar_land 
```

The availables samples are in "Echantillon" menu

## Amplitude and Pan

```ruby
sample :loop_amen, amp: 0.5, pan: -1
sleep 0.877
sample :loop_amen, amp: 2, pan: 1
```

## Rate

```ruby
sample :loop_amen, rate: 0.5
sleep 4
sample :loop_amen, rate: 2
sleep 2
```

but you can do it in reverse too
```ruby
sample :loop_amen, rate: -1
```

## ADSR (Attack, Delay, Sustain, Release)

Same as for sound, but in sample, sustain is calculated automaticely with the rest of the other values.
```ruby
print sample_duration :loop_amen
# 1.75331..
```

So if we configure a release at 0.75 ...
```ruby
sample :loop_amen, release: 0.75
```

... auto-sustain is automaticely calculated before the release, but you can manually force sustain to a value.

You can test with cymbals

```ruby
sample :drum_cymbal_open 
sleep 2
sample :drum_cymbal_open, attack: 0.01, sustain: 0, release: 0.1 
sleep 2
sample :drum_cymbal_open, attack: 0.01, sustain: 0.3, release: 0.1 
```

## Start

```ruby
sample :loop_amen, start: 0.5 
sleep 2
sample :loop_amen, start: 0.75 
```

## Finish

```ruby
sample :loop_amen, finish: 0.5
```

## Both commands !

```ruby
sample :loop_amen, start: 0.4, finish: 0.6 
```

but what happens if we invert start and finish times ?


```ruby
sample :loop_amen, start: 0.6, finish: 0.4 
```

The sample extract is played in reverse.

We can add rate

```ruby
sample :loop_amen, start: 0.5, finish: 0.7, rate: 0.2 
```

or combine it with ADSR

```ruby
sample :loop_amen, start: 0.5, finish: 0.8, rate: -0.2, attack: 0.3, release: 1 
```

## Proper sample

```ruby
# Raspberry Pi, Mac, Linux
sample "/Users/sam/Desktop/my-sound.wav"
# Windows
sample "C:/Users/sam/Desktop/my-sound.wav" 
```

## Sample packets

