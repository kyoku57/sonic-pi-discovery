# Structures
## Basics

```ruby
<command> do
  action 1
  action 2
end
```

## Return to the loop

```ruby
4.times do
  play 50
  sleep 0.5
end
8.times do
  play 55, release: 0.2
  sleep 0.25
end
4.times do
  play 50
  sleep 0.5
end 
```

## Nested loop 

```ruby 
4.times do
  sample :drum_heavy_kick
  2.times do
    sample :elec_blip2, rate: 2
    sleep 0.25
  end
  sample :elec_snare
  4.times do
    sample :drum_tom_mid_soft
    sleep 0.125
  end
end 
```

## Loop world

```ruby
loop do
  sample :loop_amen
  sleep sample_duration :loop_amen
end 
```

## If, else

```ruby
loop do
  if one_in(2)
    sample :drum_heavy_kick
  else
    sample :drum_cymbal_closed
  end
  sleep 0.5
end 
```

or with inline if

```ruby
loop do
  sample :drum_heavy_kick if one_in(2)
  sample :drum_cymbal_closed if one_in(2)
  sleep 0.5
end
```

With random chords 
```ruby
use_synth :dsaw
loop do
  play 50, amp: 0.3, release: 2
  play 53, amp: 0.3, release: 2 if one_in(2)
  play 57, amp: 0.3, release: 2 if one_in(3)
  play 60, amp: 0.3, release: 2 if one_in(4)
  sleep 1.5
end 
```


## Lists

```ruby
loop do
  play choose([50, 55, 62])
  sleep 1
end
```

To play a chord, you can write

```ruby
play [52, 55, 59], amp: 0.3
```

## Chords

```ruby
use_synth :dsaw
play [:E4,:Gs4,:B4]
sleep 1
play [:E4,:G4,:B4]
```

can be simplified with

```ruby
use_synth :dsaw
play chord(:E4, :major)
sleep 1
play chord(:E4, :minor)
```

other example
```ruby
use_synth :dsaw
play chord(:E4, :m7)
sleep 1
play chord(:E4, :minor)
sleep 1
play chord(:E4, :dim7)
sleep 1
play chord(:E4, :dom7)
```

## Scales

```ruby
play_pattern_timed scale(:c3, :major), 0.25, release: 0.1
```

add more octaves

```ruby
play_pattern_timed scale(:c3, :major, num_octaves: 3), 0.25, release: 0.1
```

```ruby
play_pattern_timed (scale :c3, :major_pentatonic, num_octaves: 3), 0.25,  release: 0.1
```

## Mix it up
```ruby
use_synth :tb303
loop do
  play choose(chord(:E3, :minor)), release: 0.3, cutoff: rrand(60, 120)
  sleep 0.25
end
```

```ruby
use_synth :tb303
loop do
  play choose(scale(:E3, :major_pentatonic)), release: 0.3, cutoff: rrand(60, 120)
  sleep 0.25
end
```

All chords/scales options are available in **Lang** option of Sonic Pi

## Rings

```ruby
(ring 52, 55, 59)
```

```ruby
[52, 55, 59].ring
```

Scales and chords are rings too.
range, bools, knit, spread

```ruby
(ring 10, 20, 30, 40, 50)
(ring 10, 20, 30, 40, 50).reverse  #=> (ring 50, 40, 30, 20, 10)
(ring 10, 20, 30, 40, 50).take(3)  #=> (ring 10, 20, 30)
(ring 10, 20, 30, 40, 50).shuffle  #=> (ring 40, 30, 10, 50, 20)
```

Multiple chains

```ruby
(ring 10, 20, 30, 40, 50) #- our initial ring
(ring 10, 20, 30, 40, 50).shuffle #- shuffles - (ring 40, 30, 10, 50, 20)
(ring 10, 20, 30, 40, 50).shuffle.drop(1) #- drop 1 - (ring 30, 10, 50, 20)
(ring 10, 20, 30, 40, 50).shuffle.drop(1).take(3) #- take 3 - (ring 30, 10, 50)
```

Rings are inmutables.