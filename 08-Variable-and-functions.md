# Variables and functions

## Functions

```ruby
use_synth :piano
define :foo do
  play :C4
  sleep 1
  play :F4
  sleep 2
end

loop do
  foo
end
```

With parameters

```ruby
define :foo do |first_note, second_note|
  play first_note
  sleep 1
  play second_note
  sleep 2
end

loop do
  use_synth :piano
  foo(:C4,:F4)
  use_synth :hollow
  foo :C4,:F4
end
```

## Variables

```ruby
sample_name = :loop_amen
sample sample_name
```

We can get the duration of a sample

```ruby
sample_name = :loop_amen

sample sample_name
sleep sample_duration(sample_name)

sample sample_name, rate: 0.5
sleep sample_duration(sample_name, rate: 0.5)

sample sample_name
sleep sample_duration(sample_name)
```

Variable can be used only in the thread.