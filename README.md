# sonic-pi-discovery

Samples of music code

## Download SonicPi

- https://sonic-pi.net/


## References

- https://sonic-pi.net/tutorial.html
- http://mathartung.xyz/siteICN/sonicpi_a1.html
- http://sonic-pi.mehackit.org/exercises/en/05-advanced-topics-1/04-external-samples.html
- http://docs.ruby-doc.com/docs/ProgrammingRuby/
- http://ruby-doc.org/
- https://www.reddit.com/r/SonicPi/
- https://gist.github.com/samaaron
- https://in-thread.sonic-pi.net/
- https://sonic-pi.mehackit.org/exercises/en/01-introduction/01-introduction.html

