# Welcome to Sonic Pi v3.2

live_loop :kick do
    tick_reset
    16.times do
        if ((spread 3,8).tick)
        then
        sample :drum_heavy_kick
        cue :kickend
        end
        sleep 0.25
    end
end


live_loop :prout do
    sync :kickend
    with_fx :echo, phase: 0.25, decay: 1 do
        #sample :drum_tom_lo_soft
    end
end

kick_value = (ring true, false, false, false,
            false, false, true, true,
            false, false, true, true,
            false, false, true, true)

live_loop :basshit do
    with_fx :reverb, room: 1, amp: 3 do
        sync :kick
        16.times do
        if kick_value.tick
        then
            sample :bass_hit_c
        end
        sleep 0.25
        end
    end
end