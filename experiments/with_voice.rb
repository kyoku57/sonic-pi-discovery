# Welcome to Sonic Pi v3.1

live_loop :voice do
    with_fx :reverb, room:1 do
        toto = (ring :c3, :d3, :e3, :c3,
                :c3, :d3, :e3, :c3,
                :e3, :f3, :g3, :g3,
                :e3, :f3, :g3, :g3)
        with_fx :autotuner, formant_ratio:1 do |a|
            live_audio :voix_de_connard
            16.times do
            control a, note: toto.tick
            sleep 0.25
            end
        end
    end
end

live_loop :voice_two do
    with_fx :autotuner, formant_ratio:1 do |a|
        16.times do
        synth :sound_in
        control a, note: [:C1, :E1,
                            :C2, :E2,
                            :C3, :E3,
                            :C4, :E4,
                            :C5, :E5,
                            :C6, :E6].tick
        sleep 0.25
        end
    end
end